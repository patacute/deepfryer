# File: deepfryer.py
# Author: Patrik I.
# Date: 08.11.2019
# Version 1.1

%matplotlib inline

import numpy as np
from PIL import Image
import matplotlib.pyplot as plt

# Sourcefile goes here
src = np.asarray(Image.open("YOUR_FILE.jpg"))

# Plot source
_ = plt.imshow(src, cmap=plt.cm.gray, vmin = 0, vmax = 255)

# Fuck up resolution
#src_mini = src[::4, ::3]       # Minor fuckup
src_mini = src[::8, ::6]        # Major fuckup

# Defining dimensions
arrSizeX = src_mini.shape[0]
arrSizeY = src_mini.shape[1]

# RGB channel split
src_b, src_g, src_r = src_mini[:, :, 0], src_mini[:, :, 1], src_mini[:, :, 2]

# Chromatic abberation
arrSizeXFragment = int(arrSizeX / 20)   # Getting relative values for dispositioning the blue channel
arrSizeYFragment = int(arrSizeY / 20)

b_shifted = src_b[arrSizeXFragment:(arrSizeX-arrSizeXFragment), 0:(arrSizeY-arrSizeYFragment)]
b_shifted = np.array(Image.fromarray(b_shifted).resize((arrSizeY, arrSizeX), Image.NEAREST))


# Channel merge
src_rgb = np.dstack((b_shifted, src_g, src_r))

# Fuck up contrast
final = np.interp(src_rgb, (src_rgb.min(), src_rgb.max()), (-1.2, 1.4))
final *= 1.3


# Plot result
_ = plt.imshow(final, cmap=plt.cm.gray, vmin = 0, vmax = 255)
